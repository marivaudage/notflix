<?php

$config['debug'] = true;

// DB connection info
$config['db.options']['user']     = 'LOCAL_DB_USER';
$config['db.options']['password'] = 'LOCAL_DB_PWD';

return $config;
