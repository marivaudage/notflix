<?php

namespace Utils;

class Misc  {

	static public function getNotflixMovies($limit, $offset = 0) {
		// build url 
		$url = 'http://www.vodkaster.com/api/film/notflix';
		$url .= '?' . http_build_query(array('offset' => $offset, 'limit' => $limit, 'mini' => 1));

		// build and run curl query
		$handle = curl_init();
		curl_setopt($handle, CURLOPT_URL, $url);
    curl_setopt($handle, CURLOPT_HEADER, 'false');
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($handle);
    $return_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    curl_close($handle);
    if($return_code != 200) 
    	return null;
    
    $out = json_decode($res);
    return empty($out->data) ? null : $out->data;
	}

}