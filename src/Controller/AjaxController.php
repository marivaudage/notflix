<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AjaxController
{
  public function indexAction(Application $app, Request $request) {
    $films = \Utils\Misc::getNotflixMovies(24, $request->get('offset', 0));
    $tpl_vars = array('films' => $films, 'base_vk' => 'http://www.vodkaster.com');
    $response = new Response($app['twig']->render('block/film-unit.html.twig', $tpl_vars));
    return $response;
  }
}

