<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class FrontController
{
  public function indexAction(Application $app) {
  	$films = \Utils\Misc::getNotflixMovies(24);
  	$tpl_vars = array('films' => $films, 'base_vk' => 'http://www.vodkaster.com');
  	
    $response = new Response($app['twig']->render('index.html.twig', $tpl_vars));
    return $response;
  }

	public function infoAction(Application $app) {
  	$response = new Response($app['twig']->render('info.html.twig'));
    return $response;
  }  
}


