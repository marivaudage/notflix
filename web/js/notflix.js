$(function() {
  // handleInfiniteScroll();

  setInterval(function() {
    handleInfiniteScroll();
  }, 200);
});

// $(window).scroll(function() {
//   handleInfiniteScroll();
// });


// Infinite scroll
var handleInfiniteScroll = function() {

  var container = $('.film-list');

  if (container.length) {
    var has_reached_bottom = $(container).offset().top + $(container).height() <= $(window).scrollTop() + $(window).height();

    // Si le bas du container est visible dans la fenêtre, charger le contenu
    if (has_reached_bottom && !$(container).data('locked')) {
      // Blocage du handler
      $(container).data('locked', 1);
      // Affichage loader
      var loader = $(container).find('.loader').removeClass('inactive');
      // Appel AJAX
      $.ajax({
        url: '/ajax',
        data: {offset: $(container).find('li.film-unit').length}
      }).done(function(response) {
        // Si il n'y a pas de film dans la réponse, c'est fini !
        if ($(response).filter('[data-id]').length == 0) {
          loader.remove();
        } else {
          // Chargement du bloc
          loader.replaceWith(response);
          _gaq.push(['_trackEvent', 'notflix_home', 'scroll']);
          // Libération du bloc
          $(container).data('locked', 0);
        }
      });
    }
  }
      

}